#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <curses.h>
#include <string.h>
#include <fcntl.h>
#include <signal.h>
#include <sys/queue.h>
#include <locale.h>

struct entry {
    TAILQ_ENTRY(entry) next;
    TAILQ_ENTRY(entry) next_ordered;

    char *data;
    int   tagged;
};

static TAILQ_HEAD(entry_list, entry) entries = TAILQ_HEAD_INITIALIZER(entries);
static TAILQ_HEAD(ordered_entry_list, entry) ordered_entries = TAILQ_HEAD_INITIALIZER(ordered_entries);
static int                           nentries;

static int    resize_request;

static int
fuzzy_match_len_r(char const *pattern,
                  size_t      pattern_len,
                  char const *text,
                  size_t      text_len,
                  int        *score)
{
    ssize_t j     = -1;
    char    flag  = 0;

    if(pattern_len == 0) {
        *score = 0;
        return 1;
    }

    while(1) {
        char *p;
        int  r_score = 0;
        char r_flag  = 0;

        p = memchr(text + j + 1, pattern[0],
                   text_len - (j + 1));
        if(p == NULL) {
            break;
        }

        j = p - text;

        r_flag = fuzzy_match_len_r(pattern + 1,
                                   pattern_len - 1,
                                   text + j + 1,
                                   text_len - (j + 1),
                                   &r_score);
        if(r_flag && r_score >= *score) {
            flag = 1;
            *score = r_score;
            if(j == 0) {
                *score += 1;
                if(pattern_len - 1 == 0 && text_len - (j + 1) == 0) {
                    //*score += 1;
                }
            }
        }
    }

    return flag;
}

static int
fuzzy_match_len(char const *pattern,
                size_t      pattern_len,
                char const *text,
                size_t      text_len)
{
    char *upper_pattern;
    char *upper_text;

    int   score = 0;
    int   ret;

    size_t i;

    upper_pattern = malloc(pattern_len);
    upper_text    = malloc(text_len);

#define UPPER(c) ((c) >= 'a' && (c) <= 'z' ? (c) + 'A' - 'a' : (c))
    for(i = 0; i < pattern_len; i++) {
        upper_pattern[i] = UPPER(pattern[i]);
    }

    for(i = 0; i < text_len; i++) {
        upper_text[i] = UPPER(text[i]);
    }

    ret = fuzzy_match_len_r(upper_pattern,
                            pattern_len,
                            upper_text,
                            text_len,
                            &score);

    free(upper_pattern);
    free(upper_text);

    if(ret == 1) {
        return score + 1;
    }

    return 0;
}

static int
fuzzy_match(char const *pattern,
            char const *text)
{
    return fuzzy_match_len(pattern, strlen(pattern),
                           text,    strlen(text));
}

void
match(char const *search)
{
    int input_len = strlen(search);
    if(input_len > 0) {
        int i;
        int max_score = input_len + 3;
        TAILQ_HEAD(entry_list, entry) search_entries[max_score];
        struct entry *cur;

        for(i = 0; i < max_score; i++) {
            TAILQ_INIT(&search_entries[i]);
        }

        for(cur = TAILQ_FIRST(&entries); cur; cur = TAILQ_NEXT(cur, next)) {
            int res = fuzzy_match(search, cur->data);
            if(res > 0) {
                TAILQ_INSERT_TAIL(&search_entries[res], cur, next_ordered);
            }
        }

        TAILQ_INIT(&ordered_entries);

        for(i = max_score - 1; i > 0; i--) {
            struct entry *next;
            for(cur = TAILQ_FIRST(&search_entries[i]); cur; cur = next) {
                next = TAILQ_NEXT(cur, next_ordered);

                TAILQ_REMOVE(&search_entries[i], cur, next_ordered);
                TAILQ_INSERT_TAIL(&ordered_entries, cur, next_ordered);
            }
        }
    } else {
        struct entry *cur;
        TAILQ_INIT(&ordered_entries);
        for(cur = TAILQ_FIRST(&entries); cur; cur = TAILQ_NEXT(cur, next)) {
            TAILQ_INSERT_TAIL(&ordered_entries, cur, next_ordered);
        }
    }
}

void sort_entries(void)
{
    TAILQ_HEAD(entry_list, entry) tmp = TAILQ_HEAD_INITIALIZER(tmp);
    struct entry *entry;

    while((entry = TAILQ_LAST(&entries, entry_list)) != NULL) {
        struct entry *cur;

        TAILQ_REMOVE(&entries, entry, next);

        for(cur = TAILQ_FIRST(&tmp); cur; cur = TAILQ_NEXT(cur, next)) {
            if(strcasecmp(entry->data, cur->data) < 0) {
                TAILQ_INSERT_BEFORE(cur, entry, next);
                break;
            }
        }
        if(cur == NULL) {
            TAILQ_INSERT_TAIL(&tmp, entry, next);
        }
    }

    while((entry = TAILQ_FIRST(&tmp)) != NULL) {
        TAILQ_REMOVE(&tmp, entry, next);
        TAILQ_INSERT_TAIL(&entries, entry, next);
    }
}

static void get_entries(int argc, char *argv[])
{
    int i;
    int fd_in = 0;

    if(!isatty(fd_in)) {
        FILE *fp = fdopen(fd_in, "r");
        char  buffer[BUFSIZ];
        char *line;

        while((line = fgets(buffer, sizeof(buffer), fp)) != 0) {
            size_t len = strlen(line);

            struct entry *entry = calloc(1, sizeof(*entry));
            entry->data = strndup(line, line[len - 1] == '\n' ? len - 1 : len);
            nentries++;

            TAILQ_INSERT_TAIL(&entries, entry, next);
            TAILQ_INSERT_TAIL(&ordered_entries, entry, next_ordered);
        }

        fclose(fp);
        close(fd_in);

        open("/dev/tty", O_RDONLY);
    }
    for(i = 0; i < argc; i++) {
        struct entry *entry = calloc(1, sizeof(*entry));
        entry->data = strdup(argv[i]);
        nentries++;

        TAILQ_INSERT_TAIL(&entries, entry, next);
        TAILQ_INSERT_TAIL(&ordered_entries, entry, next_ordered);
    }
}

static void cleanup_entries(void)
{
}

static void sigwinch_handler(int signum)
{
    (void)signum;
    resize_request = 1;
}

static void sigwinch_install(void)
{
    struct sigaction sa;

    sa.sa_handler = sigwinch_handler;
    sigemptyset(&sa.sa_mask);
    sa.sa_flags = 0;

    sigaction(SIGWINCH, &sa, NULL);
}

int
main(int argc, char *argv[])
{
    WINDOW *wmain;
    int     height;
    int     width;
    int     y;
    int     pos;
    int     i;
    int     start_pos;
    int     current_pos;
    int     c;
    char    search[256];
    int     nsearch = 0;
    int     output_index = 0;
    int   (*strcompare)(const char *, const char *, size_t) = strncmp;
    int     clear_search = 0;
    int     reset_search = 0;
    int     command = 0;

    int     opt = 1;

    for(i = 1; i < argc; i++) {
        if(strcmp(argv[i], "-n") == 0) {
            output_index = 1;

            opt++;
        } else if(strcmp(argv[i], "-i") == 0) {
            strcompare = strncasecmp;

            opt++;
        } else
            break;
    }

    setlocale(LC_ALL, "");

    get_entries(argc - opt, argv + opt);

    if(nentries < 1)
        exit(EXIT_FAILURE);

    sigwinch_install();

    wmain = initscr();
    cbreak();
    noecho();
    curs_set(0);

    nonl();
    intrflush(stdscr, FALSE);
    keypad(stdscr, TRUE);

    start_color();

    init_pair(1, COLOR_BLACK, COLOR_YELLOW);
    init_pair(2, COLOR_YELLOW, COLOR_BLACK);
    init_pair(3, COLOR_BLACK, COLOR_RED);

    resize_request = 1;

    start_pos = 0;
    current_pos = 0;
    int not_found = 0;

    search[0] = 0;

    struct entry *selected = NULL;

    while(1) {
        int set_command = 0;

        if(resize_request) {
            endwin();
            refresh();
            getmaxyx(wmain, height, width);
            resize_request = 0;
        }
        struct entry *cur;

        int i = 0;
        for(cur = TAILQ_FIRST(&ordered_entries); cur; cur = TAILQ_NEXT(cur, next_ordered)) {
            if(i == start_pos) {
                break;
            }
            i++;
        }
        for(y = 1; y < height && cur; y++) {
            int tagged = cur->tagged;

            if(i == current_pos) {
                selected = cur;
                attron(COLOR_PAIR(1));
            } else if(tagged) {
                attron(COLOR_PAIR(2));
                attron(A_BOLD);
            }
            mvhline(y, 0, ' ', width);
            mvprintw(y, 0, "%.*s", width, cur->data);

            if(i == current_pos) {
                attroff(COLOR_PAIR(1));
            } else if(tagged) {
                attroff(COLOR_PAIR(2));
                attroff(A_BOLD);
            }

            cur = TAILQ_NEXT(cur, next_ordered);
            i++;
        }
        move(y, 0);

        clrtobot();
        if(nsearch) {
            if(not_found) {
                attron(COLOR_PAIR(3));
                attron(A_BOLD);
            }
            mvprintw(0, 0, "%s", search);
            if(not_found) {
                attroff(COLOR_PAIR(3));
                attroff(A_BOLD);
            }
            clrtoeol();
        }

        refresh();

        c = getch();
        switch(c) {
        case KEY_UP:
            current_pos--;
            clear_search = 1;
            break;
        case KEY_DOWN:
            current_pos++;
            clear_search = 1;
            break;
        case KEY_PPAGE:
            current_pos -= height - 2;
            clear_search = 1;
            break;
        case KEY_NPAGE:
            current_pos += height - 2;
            clear_search = 1;
            break;
        case ' ':
            if(!nsearch) {
                selected->tagged = !selected->tagged;
                current_pos++;
                clear_search = 1;
            } else if(nsearch < (int)sizeof(search) - 1) {
                search[nsearch++] = c;
                search[nsearch] = '\0';
            }
            break;
        case '\r':
            goto out;
            break;
        case 033:
            current_pos = -1;
            goto out;
            break;
        case KEY_BACKSPACE:
            if(nsearch)
                nsearch--;
            if(nsearch == 0) {
                mvprintw(0, 0, "  ");
                reset_search = 1;
            }
            search[nsearch] = '\0';
            break;
        case '\\':
            set_command = 1;
            break;
        case -1:
            continue;
            break;
        case KEY_RESIZE:
            break;
        default:
            if(command == 1) {
                if(c == 's') {
                    sort_entries();
                    match(search);
                }
            } else if(nsearch < (int)sizeof(search) - 1) {
                search[nsearch++] = c;
                search[nsearch] = '\0';
                current_pos = 0;
            }
            break;
        }
        command = !command && set_command;

        if(clear_search) {
            clear_search = 0;
            move(height - 1, width - nsearch - 2);
            nsearch = 0;
        }

        if(nsearch || reset_search) {
            match(search);
            reset_search = 0;
        }
        if(current_pos < 0) {
            current_pos = 0;
        }
        if(current_pos >= nentries) {
            current_pos = nentries - 1;
        }
        if(current_pos - start_pos < 0) {
            clear();
            start_pos += current_pos - start_pos;
        }
        if(current_pos - start_pos > height - 2) {
            clear();
            start_pos += current_pos - start_pos - (height - 2);
        }

        if(start_pos < 0) {
            clear();
            start_pos = 0;
        }
        if(start_pos >= nentries) {
            clear();
            start_pos = nentries - 1;
        }
    }

out:
    endwin();

    if(current_pos != -1) {
        int has_tag = 0;
        int pos;
        struct entry *cur;
        for(pos = 0, cur = TAILQ_FIRST(&entries); cur; cur = TAILQ_NEXT(cur, next), pos++) {
            if(cur->tagged) {
                has_tag = 1;
                if(output_index)
                    fprintf(stderr, "%d\n", pos);
                else
                    fprintf(stderr, "%s\n", cur->data);
            }
        }
        if(!has_tag) {
            if(output_index)
                fprintf(stderr, "%d\n", current_pos);
            else
                fprintf(stderr, "%s\n", selected->data);
        }
    }

    cleanup_entries();

    return 0;
}
